VERSION=0.8.1

package:
	./silex pack silex.sh

install:
	cp -f silex /usr/sbin/
	mkdir -p /etc/silex.d /usr/share/doc/silex/
	cp -f mirrorlist /etc/silex.d/
	cp -f Changelog /usr/share/doc/silex/
	cp -f License /usr/share/doc/silex/
	cp -f README /usr/share/doc/silex/

uninstall:
	rm -f /usr/sbin/silex
	rm -fr /etc/silex.d/
	rm -fr /usr/share/doc/silex/

git-commit:
	git add .
	git commit -m "new commit"
